library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity inc_gray is

    generic (n : natural := 10;
		inc : natural := 1);
    port
    (
		gray_in  : in std_logic_vector(n-1 downto 0);
		bray_out  : out std_logic_vector(n-1 downto 0)
    );
end entity;

architecture arch_1 of inc_gray is

    signal bin : std_logic_vector(n-1 downto 0);
    signal bin_inc : std_logic_vector(n-1 downto 0); --n-1 para se manter o mesmo valor, pensar em carry out
    signal bin_uns : std_logic_vector(n-1 downto 0);
begin

    -- convertendo para binario (gray to bin)
    g2bin: for i in gray_in'left-1 downto 0 generate
        bin(i) <= gray_in(i) xor bin(i+1);
    end generate g2bin;
    bin(n-1) <= gray_in(n-1);

    bin_uns <= unsigned(bin)+inc;

    bin_inc <= std_logic_vector(bin_uns);

    b2gray: for i in bin'left-1 downto 0 generate
        bray_out(i) <= bin_inc(i) xor bin_inc(i+1);
    end generate b2gray;
    bray_out(n-1) <= bin_inc(n-1);

end architecture;
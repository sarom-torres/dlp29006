# DLP-29006

Repositório utilizado para exemplos e exercícios na disciplina de **Dispositivos Lógicos Programáveis** (DLP29006) do curso de Engenharia de Telecomunicações.

## Conteúdos abordados neste Repositório

### VHDL
- tipos de dados, operadores e atributos
- código VHDL concorrente
- circuitos combinacionais
- circuitos aritméticos
- código VHDL sequencial
- projetos hierárquicos
- simulação e uso de testbench
- projeto de maquinas de estado



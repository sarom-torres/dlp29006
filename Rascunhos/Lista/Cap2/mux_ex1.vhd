library IEEE;
use IEEE.std_logic_1164.all;

entity mux_ex1 is
    port ( a, b : in std_logic_vector(7 downto 0);
        sel : in std_logic_vector (1 downto 0);
        x : out std_logic_vector (7 downto 0)        
    ); 
end entity mux_ex1;

architecture example of mux_ex1 is
    
begin
    process (a,b,sel) -- O que se deve colocar nessa parte do parenteses?Porque foi usado o process?
    begin
        if (sel = "00") then
            x <= "00000000";
        elsif(sel = "01") then
            x <= a;
        elsif (sel = "10") then
            x <= b;
        else
            x <= "ZZZZZZZZ";
        end if;
    end process;
end architecture example;

architecture example2 of mux_ex1 is
    
begin
    x <= "00000000" when sel = "00" else
        a when sel = "01" else
        b when sel = "10" else
        "ZZZZZZZZ";    
end architecture example2;


configuration ifsc of mux_ex1 is
	for example2 end for;
end configuration;





library IEEE;
use IEEE.std_logic_1164.all;


entity ports_ex2 is
    port (
        clk : in std_logic;
        a, b, c : in bit;
		reg_z : out bit 
    );
end entity;

architecture v1 of ports_ex2 is
	 signal x, y, z :  bit;
begin
    x <= a nand b;
    y <= c or x;
    z <= a xor y;  

    process (clk)
        begin
            if (clk'event and clk = "1") then
                reg_z <=  z;
            end if;
        end;    
end architecture v1;
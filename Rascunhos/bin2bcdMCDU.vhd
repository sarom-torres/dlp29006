library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity bin2bcdMCDU is
	port (
		X_bin	    : in  std_logic_vector(13 downto 0);  --  0000 a 9999
		M_bcd		: out std_logic_vector(3 downto 0);  --  Milhar
		C_bcd		: out std_logic_vector(3 downto 0);  --  Centena
		D_bcd		: out std_logic_vector(3 downto 0);  --  Dezena
		U_bcd		: out std_logic_vector(3 downto 0)); --  Unidade
 
end entity;
 
architecture example of bin2bcdMCDU is
--declaração de sinais auxiliares

    signal X_bin_uns : unsigned(13 downto 0);
	signal M_bcd_uns : unsigned(3 downto 0);
	signal C_bcd_uns : unsigned(3 downto 0);
	signal D_bcd_uns : unsigned(3 downto 0);
	signal U_bcd_uns : unsigned(3 downto 0);

	signal X_temp_uns : unsigned(9 downto 0);
	signal C_temp_uns : unsigned(6 downto 0);
	signal D_temp_uns : unsigned(3 downto 0);
 
begin
--descrição do hardware
	X_bin_uns <= unsigned(X_bin);

	M_bcd_uns <= resize(X_bin_uns MOD 10,4);
	X_temp_uns <= resize(X_bin_uns REM 10,10);
	
	C_bcd_uns <= resize(X_temp_uns MOD 10,4);
	C_temp_uns <= resize(X_temp_uns REM 10,7);

	D_bcd_uns <= resize(C_temp_uns MOD 10,4);
	D_temp_uns <= resize(C_temp_uns REM 10,4);

	U_bcd_uns <= D_temp_uns MOD 10;

--	U_bcd_uns <= resize(X_bin_uns/10,4);
--	D_bcd_uns <= U_bcd_uns/10;
--	C_bcd_uns <= D_bcd_uns/10;
--	M_bcd_uns <= C_bcd_uns/10;

	U_bcd <= std_logic_vector(U_bcd_uns);
	D_bcd <= std_logic_vector(D_bcd_uns);
	C_bcd <= std_logic_vector(C_bcd_uns);
	M_bcd <= std_logic_vector(M_bcd_uns);
 
end architecture;
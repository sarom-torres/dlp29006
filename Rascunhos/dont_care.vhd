--Figure 3.4a depicts a circuit whose input (x) and output ( y) are 2-bit signals for which
--two sets of specifications are given in the truth tables of figures 3.4b–c. In the former, all
--outputs are specified with '0's and '1's, while in the latter there is a ‘‘don’t care’’ output
--(y 1⁄4 "--"). Design this circuit by hand, then write a VHDL code for each truth table and
--observe the consequences of employing the logic value ' -' in the code.

library IEEE;
use IEEE.std_logic_1164.all;

entity dont_care is
    port (
        x: in std_logic_vector (1 downto 0);
        y: out std_logic_vector (1 downto 0)
    );
end entity dont_care;

architecture circuit of dont_care is
begin
    y <= "00" when x = "00" else
         "10" when x = "01" else
         "01" when x = "10" else
         "--";   
end architecture;
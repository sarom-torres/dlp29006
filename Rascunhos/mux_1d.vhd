

-- Essas linhas devem estar em um arquivo  separado
-- File: meu_pkg.vhd
package meu_pkg is
  constant Nbit : natural := 2;
  type array1Dx1D is array (natural range <>) of bit_vector(Nbit-1 downto 0);
  type array2D is array (natural range <>, natural range <>) of bit;
end package;

--File: mux1Dx1D.vhd
use work.meu_pkg.all;

entity mux1Dx1D is
  generic (M : natural := 4);
  port (
    x   : in array1Dx1D(M-1 downto 0);
    sel : in integer range 0 to M-1;
    y   : out bit_vector(Nbit-1 downto 0)
  );
end entity;

architecture ifsc of mux1Dx1D is
begin
  y <= x(sel);
end architecture;
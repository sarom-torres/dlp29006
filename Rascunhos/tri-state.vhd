--As already mentioned, a fundamental synthesizable value is 'Z' (high impedance), which is
--needed to create tri-state bu¤ers, like that depicted in figure 3.3 (see its truth table). Write a
--VHDL code from which this circuit can be inferred. pg 50


library IEEE;
use IEEE.std_logic_1164.all;

entity tri_state is
    port (
        input, ena : in std_logic;
        output: out std_logic        
    );
end entity tri_state;

architecture buff of tri_state is
begin
    output <= 'Z' when ena = '0' else input;
--    output <= input when ena = '1';  --pq nao aceita dessa forma
    
end architecture buff;
library IEEE;
use IEEE.std_logic_1164.all;

entity paridade is
    generic (n: integer := 8); 
    port (
        x : in bit_vector(n-1 downto 0);
        y : out bit        
    );
end entity paridade;

architecture verificador of paridade is
    signal contador : bit_vector(n-1 downto 0); -- crio fio que faz as ligaes doas portas xor
begin
    contador(0) <= x(0); --inicio em zero o contador com o valor de do primeiro bit da entrada
    gen: for i in 1 to n-1 generate
        contador(i) <= contador(i-1) xor contador(i); --xor entre o valor de dois bits (00-0, 01-1, 10-1, 11-0)
    end generate;
    y <= contador(i);
    
end architecture;